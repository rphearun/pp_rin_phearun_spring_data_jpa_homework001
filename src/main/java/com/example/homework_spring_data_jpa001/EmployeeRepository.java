package com.example.homework_spring_data_jpa001;



import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class EmployeeRepository {

    @PersistenceContext
    EntityManager em ;

    @Transactional
    public void addEmp(Employee employee){
        em.persist(employee);
    }

    @Transactional
    public Employee findById(Long id) {
        return em.find(Employee.class,id);
    }

    @Transactional
    public void deleteEmployee(Long id) {
        Employee employee = em.find(Employee.class,id);
        em.remove(employee);
    }

    @Transactional
    public Employee detach(Employee employee) {
        em.detach(employee);
        employee.setFirstName("antony");
        employee.setLastName("martial");
        em.merge(employee);
        em.flush();
        return null;
    }
}
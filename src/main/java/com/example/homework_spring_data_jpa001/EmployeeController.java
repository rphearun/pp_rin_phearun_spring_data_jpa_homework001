package com.example.homework_spring_data_jpa001;


import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api/test")
@AllArgsConstructor
public class EmployeeController {
    private final EmployeeRepository employeeRepository;


  @PostMapping("/add")
  public ResponseEntity<?> addEmployee(){
      Employee employee = new Employee(null,"jame","koy","jameKoy@gmail.com",new Date(),null);
        employeeRepository.addEmp(employee);
        return ResponseEntity.ok("Add successfully");
  }
  @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
    return ResponseEntity.ok(employeeRepository.findById(id));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteEmployee(@PathVariable Long id){
    employeeRepository.deleteEmployee(id);
    return ResponseEntity.ok("Delete successfully");
  }

  @PostMapping("/detach")
  public ResponseEntity<?> detachData(){
    Employee employee = new Employee(null,"jing","koy","jingKoy1@gmail.com",new Date(),null);
    employeeRepository.detach(employee);
    return  ResponseEntity.ok(employee);
  }



}

package com.example.homework_spring_data_jpa001;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class HomeworkSpringDataJpa001Application {
    public static void main(String[] args) {
        SpringApplication.run(HomeworkSpringDataJpa001Application.class, args);
    }





}

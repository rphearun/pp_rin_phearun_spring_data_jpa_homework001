package com.example.homework_spring_data_jpa001;


import jakarta.persistence.*;
import lombok.*;

import java.util.Date;


@Entity
@Table(name = "employees")
@AllArgsConstructor

@Data
@NoArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email",unique = true,length =50)
    private String email ;
    @Column(name = "brith_date")
    @Temporal(TemporalType.DATE)
    private Date brithDate;

    @Transient
    private String temp;

}
